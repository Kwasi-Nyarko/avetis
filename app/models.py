from flask import current_app
from . import db
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from . import login_manager
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    users = db.relationship('User', backref='role', lazy='dynamic')

    def __repr__(self):
        return '<Role %r>' % self.name


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    username = db.Column(db.String(64), unique=False, index=True)
    password_hash = db.Column(db.String(128))
    roles = db.Column(db.String(64), unique=False, index=True)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))


class Requisitions:
    def __init__(self):
        pass
    __tablename__ = 'Requisition'
    project_id = db.Column(db.Integer, primary_key=True)
    project_name = db.Column(db.String(64), unique=False, index=True)
    client_name = db.Column(db.String(64), unique=True, index=True)
    requested_by = db.Column(db.String(64), unique=False, index=True)
    approved_by = db.Column(db.String(64), unique=True, index=True)
    chargetoAcc = db.Column(db.String(64), unique=False, index=True)
    delivered_to = db.Column(db.String(64), unique=True, index=True)
    duration = db.Column(db.String(64), unique=False, index=True)


    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User %r>' % self.username
