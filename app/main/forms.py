from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectMultipleField, PasswordField
from wtforms.validators import InputRequired, Length, Email, Regexp

Accounts = [('Main_acc.', 'General Accounts'), ('Project_acc', 'Project Account'), ('Financial_Office', 'Finances'),
            ('Other', 'Others')]


class NameForm(FlaskForm):
    name = StringField('What is your name?', validators=[InputRequired()])
    submit = SubmitField('Submit')


class RequisitionForm(FlaskForm):
    requested_by = StringField('Requested by', validators=[InputRequired(), Length(1, 64),
                                                           Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                                                  'Usernames must have only letters, '
                                                                  'numbers, dots or underscores')])
    approved_by = StringField('Approved by', validators=[InputRequired(), Length(1, 64),
                                                         Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                                                'Usernames must have only letters, '
                                                                'numbers, dots or underscores')])
    chargetoAcc = SelectMultipleField('Select role on team', choices=Accounts)

    delivered_to = StringField('Charged to Account', validators=[InputRequired(), Length(1, 64),
                                                                 Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                                                        'Usernames must have only letters, '
                                                                        'numbers, dots or underscores')])
    project_name = StringField('Project Name', validators=[InputRequired(), Length(1, 64),
                                                                 Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                                                        'Usernames must have only letters, '
                                                                        'numbers, dots or underscores')])
    client_name = StringField('Client Name', validators=[InputRequired(), Length(1, 64),
                                                                 Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                                                        'Usernames must have only letters, '
                                                                        'numbers, dots or underscores')])
    budget = StringField('Total Budget', validators=[InputRequired(), Length(1, 64),
                                                                     Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                                                            'Usernames must have only letters, '
                                                                            'numbers, dots or underscores')])
    duration = StringField('Duration', validators=[InputRequired(), Length(1, 64),
                                                                     Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                                                            'Usernames must have only letters, '
                                                                            'numbers, dots or underscores')])
    submit = SubmitField('Submit')
