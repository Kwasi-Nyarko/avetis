from flask import render_template, redirect, url_for
from ..models import Requisitions
from app import db
from app.main.forms import RequisitionForm
from . import main


@main.route('/')
def index():
    return render_template('index.html')


@main.route('/example')
def mapview():
    return render_template('example.html')


@main.route('/map')
def items():
    return render_template('map.html')


@main.route('/requisition')
def requisition():
    form = RequisitionForm()
    if form.validate_on_submit():
        requisition = Requisitions(project_name=form.project_name.data,
                                   client_name=form.client_name.data,
                                   requested_by=form.requested_by.data,
                                   approved_by=form.approved_by.data,
                                   chargetoAcc=form.chargetoAcc.data,
                                   delivered_to=form.delivered_to.data,
                                   duration=form.duration.data
                                   )
        db.session.add(requisition)
        db.session.commit()
        return redirect(url_for('main.example'))
    return render_template('request_requisition.html', form=form)
