Dear {{ user.username }},

Welcome to WbAp!

To confirm your account please click on the following link:

{{ url_for('auth.confirm', token=token, _external=True) }}

Sincerely,

The CodeX Team

Note: replies to this email address are not monitored.